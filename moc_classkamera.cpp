/****************************************************************************
** Meta object code from reading C++ file 'classkamera.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "classkamera.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'classkamera.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ClassKamera[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   34,   34,   34, 0x05,
      35,   60,   34,   34, 0x05,

 // slots: signature, parameters, type, tag, flags
      69,   60,   34,   34, 0x0a,
      88,  110,   34,   34, 0x0a,
     119,   34,   34,   34, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ClassKamera[] = {
    "ClassKamera\0resultCameraMath(Mat)\0\0"
    "signalRunCamera(QString)\0namaFile\0"
    "runKamera(QString)\0statusKamera(QString)\0"
    "perintah\0timerKamera()\0"
};

void ClassKamera::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ClassKamera *_t = static_cast<ClassKamera *>(_o);
        switch (_id) {
        case 0: _t->resultCameraMath((*reinterpret_cast< Mat(*)>(_a[1]))); break;
        case 1: _t->signalRunCamera((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->runKamera((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->statusKamera((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->timerKamera(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ClassKamera::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ClassKamera::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ClassKamera,
      qt_meta_data_ClassKamera, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ClassKamera::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ClassKamera::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ClassKamera::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ClassKamera))
        return static_cast<void*>(const_cast< ClassKamera*>(this));
    return QObject::qt_metacast(_clname);
}

int ClassKamera::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}

// SIGNAL 0
void ClassKamera::resultCameraMath(Mat _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void ClassKamera::signalRunCamera(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
