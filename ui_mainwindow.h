/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QTextEdit>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include <QtWebKit/QWebView>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGroupBox *groupBox;
    QPushButton *pbStartRecord;
    QPushButton *pbStopRecord;
    QLabel *label_21;
    QLabel *lblStatusKamera;
    QGroupBox *groupBox_2;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_18;
    QLabel *label_19;
    QLabel *lblVal1;
    QLabel *lblVal2;
    QGroupBox *groupBox_5;
    QLabel *label_16;
    QPushButton *pbReadInput;
    QTextEdit *txtPin_INPUT;
    QTextEdit *txtStatusInput;
    QLabel *label_17;
    QLabel *lblVal3;
    QLabel *lblVal4;
    QGroupBox *groupBox_3;
    QLabel *label_13;
    QLabel *label_14;
    QGroupBox *groupBox_4;
    QLabel *label_15;
    QPushButton *pbOn;
    QPushButton *pbOff;
    QTextEdit *txtPin;
    QGroupBox *groupBox_7;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout_9;
    QPushButton *pbOn_2;
    QPushButton *pbOff_2;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_10;
    QPushButton *pbOn_3;
    QPushButton *pbOff_3;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *horizontalLayout_11;
    QPushButton *pbOn_4;
    QPushButton *pbOff_4;
    QWidget *horizontalLayoutWidget_4;
    QHBoxLayout *horizontalLayout_12;
    QPushButton *pbOn_5;
    QPushButton *pbOff_5;
    QWidget *horizontalLayoutWidget_5;
    QHBoxLayout *horizontalLayout_13;
    QPushButton *pbOn_6;
    QPushButton *pbOff_6;
    QWidget *horizontalLayoutWidget_6;
    QHBoxLayout *horizontalLayout_14;
    QPushButton *pbOn_7;
    QPushButton *pbOff_7;
    QWidget *horizontalLayoutWidget_7;
    QHBoxLayout *horizontalLayout_15;
    QPushButton *pbOn_8;
    QPushButton *pbOff_8;
    QLabel *lblstatusIjinMasuk;
    QLabel *label_22;
    QLabel *lblstatusTerkunci;
    QLabel *label_41;
    QWidget *tab_2;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_7;
    QLabel *lblStatusEmail;
    QHBoxLayout *horizontalLayout_6;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_5;
    QLineEdit *username;
    QLabel *label_6;
    QLineEdit *password;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_8;
    QLineEdit *sender;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_9;
    QLineEdit *recipients;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_10;
    QLineEdit *subject;
    QPushButton *sendEmail;
    QTextEdit *texteditor;
    QWidget *tab_3;
    QWebView *webView;
    QPushButton *pbStop;
    QPushButton *pbStart;
    QLabel *lblKameraLabel;
    QGroupBox *groupBox_8;
    QTextEdit *txtIPCAM;
    QLabel *label_20;
    QPushButton *btnReloadIPCAm;
    QGroupBox *groupBox_6;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *host;
    QLabel *label_2;
    QSpinBox *port;
    QCheckBox *ssl;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_4;
    QCheckBox *auth;
    QWidget *layoutWidget_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_3;
    QPushButton *addAttachment;
    QListWidget *attachments;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(912, 652);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setGeometry(QRect(-10, 10, 881, 591));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        groupBox = new QGroupBox(tab);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(0, 30, 201, 91));
        pbStartRecord = new QPushButton(groupBox);
        pbStartRecord->setObjectName(QString::fromUtf8("pbStartRecord"));
        pbStartRecord->setGeometry(QRect(10, 30, 81, 25));
        pbStopRecord = new QPushButton(groupBox);
        pbStopRecord->setObjectName(QString::fromUtf8("pbStopRecord"));
        pbStopRecord->setGeometry(QRect(10, 60, 81, 25));
        label_21 = new QLabel(groupBox);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setGeometry(QRect(110, 30, 57, 14));
        lblStatusKamera = new QLabel(groupBox);
        lblStatusKamera->setObjectName(QString::fromUtf8("lblStatusKamera"));
        lblStatusKamera->setGeometry(QRect(100, 60, 101, 16));
        groupBox_2 = new QGroupBox(tab);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(210, 30, 231, 131));
        label_11 = new QLabel(groupBox_2);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(10, 46, 91, 20));
        label_12 = new QLabel(groupBox_2);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(10, 66, 91, 20));
        label_18 = new QLabel(groupBox_2);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setGeometry(QRect(10, 90, 91, 20));
        label_19 = new QLabel(groupBox_2);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setGeometry(QRect(10, 110, 91, 20));
        lblVal1 = new QLabel(groupBox_2);
        lblVal1->setObjectName(QString::fromUtf8("lblVal1"));
        lblVal1->setGeometry(QRect(100, 40, 91, 20));
        lblVal2 = new QLabel(groupBox_2);
        lblVal2->setObjectName(QString::fromUtf8("lblVal2"));
        lblVal2->setGeometry(QRect(100, 60, 91, 20));
        groupBox_5 = new QGroupBox(groupBox_2);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(0, 20, 91, 16));
        label_16 = new QLabel(groupBox_5);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setGeometry(QRect(10, 30, 91, 16));
        pbReadInput = new QPushButton(groupBox_5);
        pbReadInput->setObjectName(QString::fromUtf8("pbReadInput"));
        pbReadInput->setGeometry(QRect(110, 90, 83, 25));
        txtPin_INPUT = new QTextEdit(groupBox_5);
        txtPin_INPUT->setObjectName(QString::fromUtf8("txtPin_INPUT"));
        txtPin_INPUT->setGeometry(QRect(100, 20, 104, 31));
        txtStatusInput = new QTextEdit(groupBox_5);
        txtStatusInput->setObjectName(QString::fromUtf8("txtStatusInput"));
        txtStatusInput->setGeometry(QRect(100, 50, 104, 31));
        label_17 = new QLabel(groupBox_5);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setGeometry(QRect(10, 60, 91, 16));
        lblVal3 = new QLabel(groupBox_2);
        lblVal3->setObjectName(QString::fromUtf8("lblVal3"));
        lblVal3->setGeometry(QRect(90, 90, 91, 20));
        lblVal4 = new QLabel(groupBox_2);
        lblVal4->setObjectName(QString::fromUtf8("lblVal4"));
        lblVal4->setGeometry(QRect(90, 110, 91, 20));
        groupBox_3 = new QGroupBox(tab);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(440, 30, 231, 131));
        label_13 = new QLabel(groupBox_3);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(10, 30, 91, 16));
        label_14 = new QLabel(groupBox_3);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(10, 50, 91, 16));
        groupBox_4 = new QGroupBox(tab);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(220, 260, 231, 91));
        label_15 = new QLabel(groupBox_4);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setGeometry(QRect(10, 30, 91, 16));
        pbOn = new QPushButton(groupBox_4);
        pbOn->setObjectName(QString::fromUtf8("pbOn"));
        pbOn->setGeometry(QRect(20, 60, 83, 25));
        pbOff = new QPushButton(groupBox_4);
        pbOff->setObjectName(QString::fromUtf8("pbOff"));
        pbOff->setGeometry(QRect(110, 60, 83, 25));
        txtPin = new QTextEdit(groupBox_4);
        txtPin->setObjectName(QString::fromUtf8("txtPin"));
        txtPin->setGeometry(QRect(100, 20, 104, 31));
        groupBox_7 = new QGroupBox(tab);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        groupBox_7->setGeometry(QRect(0, 130, 201, 221));
        horizontalLayoutWidget = new QWidget(groupBox_7);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(20, 20, 179, 27));
        horizontalLayout_9 = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(0, 0, 0, 0);
        pbOn_2 = new QPushButton(horizontalLayoutWidget);
        pbOn_2->setObjectName(QString::fromUtf8("pbOn_2"));

        horizontalLayout_9->addWidget(pbOn_2);

        pbOff_2 = new QPushButton(horizontalLayoutWidget);
        pbOff_2->setObjectName(QString::fromUtf8("pbOff_2"));

        horizontalLayout_9->addWidget(pbOff_2);

        horizontalLayoutWidget_2 = new QWidget(groupBox_7);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(20, 40, 168, 27));
        horizontalLayout_10 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(0, 0, 0, 0);
        pbOn_3 = new QPushButton(horizontalLayoutWidget_2);
        pbOn_3->setObjectName(QString::fromUtf8("pbOn_3"));

        horizontalLayout_10->addWidget(pbOn_3);

        pbOff_3 = new QPushButton(horizontalLayoutWidget_2);
        pbOff_3->setObjectName(QString::fromUtf8("pbOff_3"));

        horizontalLayout_10->addWidget(pbOff_3);

        horizontalLayoutWidget_3 = new QWidget(groupBox_7);
        horizontalLayoutWidget_3->setObjectName(QString::fromUtf8("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(20, 70, 168, 27));
        horizontalLayout_11 = new QHBoxLayout(horizontalLayoutWidget_3);
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(0, 0, 0, 0);
        pbOn_4 = new QPushButton(horizontalLayoutWidget_3);
        pbOn_4->setObjectName(QString::fromUtf8("pbOn_4"));

        horizontalLayout_11->addWidget(pbOn_4);

        pbOff_4 = new QPushButton(horizontalLayoutWidget_3);
        pbOff_4->setObjectName(QString::fromUtf8("pbOff_4"));

        horizontalLayout_11->addWidget(pbOff_4);

        horizontalLayoutWidget_4 = new QWidget(groupBox_7);
        horizontalLayoutWidget_4->setObjectName(QString::fromUtf8("horizontalLayoutWidget_4"));
        horizontalLayoutWidget_4->setGeometry(QRect(20, 100, 168, 27));
        horizontalLayout_12 = new QHBoxLayout(horizontalLayoutWidget_4);
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalLayout_12->setContentsMargins(0, 0, 0, 0);
        pbOn_5 = new QPushButton(horizontalLayoutWidget_4);
        pbOn_5->setObjectName(QString::fromUtf8("pbOn_5"));

        horizontalLayout_12->addWidget(pbOn_5);

        pbOff_5 = new QPushButton(horizontalLayoutWidget_4);
        pbOff_5->setObjectName(QString::fromUtf8("pbOff_5"));

        horizontalLayout_12->addWidget(pbOff_5);

        horizontalLayoutWidget_5 = new QWidget(groupBox_7);
        horizontalLayoutWidget_5->setObjectName(QString::fromUtf8("horizontalLayoutWidget_5"));
        horizontalLayoutWidget_5->setGeometry(QRect(20, 130, 168, 27));
        horizontalLayout_13 = new QHBoxLayout(horizontalLayoutWidget_5);
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        horizontalLayout_13->setContentsMargins(0, 0, 0, 0);
        pbOn_6 = new QPushButton(horizontalLayoutWidget_5);
        pbOn_6->setObjectName(QString::fromUtf8("pbOn_6"));

        horizontalLayout_13->addWidget(pbOn_6);

        pbOff_6 = new QPushButton(horizontalLayoutWidget_5);
        pbOff_6->setObjectName(QString::fromUtf8("pbOff_6"));

        horizontalLayout_13->addWidget(pbOff_6);

        horizontalLayoutWidget_6 = new QWidget(groupBox_7);
        horizontalLayoutWidget_6->setObjectName(QString::fromUtf8("horizontalLayoutWidget_6"));
        horizontalLayoutWidget_6->setGeometry(QRect(20, 160, 168, 27));
        horizontalLayout_14 = new QHBoxLayout(horizontalLayoutWidget_6);
        horizontalLayout_14->setSpacing(6);
        horizontalLayout_14->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        horizontalLayout_14->setContentsMargins(0, 0, 0, 0);
        pbOn_7 = new QPushButton(horizontalLayoutWidget_6);
        pbOn_7->setObjectName(QString::fromUtf8("pbOn_7"));

        horizontalLayout_14->addWidget(pbOn_7);

        pbOff_7 = new QPushButton(horizontalLayoutWidget_6);
        pbOff_7->setObjectName(QString::fromUtf8("pbOff_7"));

        horizontalLayout_14->addWidget(pbOff_7);

        horizontalLayoutWidget_7 = new QWidget(groupBox_7);
        horizontalLayoutWidget_7->setObjectName(QString::fromUtf8("horizontalLayoutWidget_7"));
        horizontalLayoutWidget_7->setGeometry(QRect(20, 190, 168, 27));
        horizontalLayout_15 = new QHBoxLayout(horizontalLayoutWidget_7);
        horizontalLayout_15->setSpacing(6);
        horizontalLayout_15->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        horizontalLayout_15->setContentsMargins(0, 0, 0, 0);
        pbOn_8 = new QPushButton(horizontalLayoutWidget_7);
        pbOn_8->setObjectName(QString::fromUtf8("pbOn_8"));

        horizontalLayout_15->addWidget(pbOn_8);

        pbOff_8 = new QPushButton(horizontalLayoutWidget_7);
        pbOff_8->setObjectName(QString::fromUtf8("pbOff_8"));

        horizontalLayout_15->addWidget(pbOff_8);

        lblstatusIjinMasuk = new QLabel(tab);
        lblstatusIjinMasuk->setObjectName(QString::fromUtf8("lblstatusIjinMasuk"));
        lblstatusIjinMasuk->setGeometry(QRect(350, 180, 91, 20));
        label_22 = new QLabel(tab);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setGeometry(QRect(220, 180, 121, 21));
        lblstatusTerkunci = new QLabel(tab);
        lblstatusTerkunci->setObjectName(QString::fromUtf8("lblstatusTerkunci"));
        lblstatusTerkunci->setGeometry(QRect(350, 200, 91, 20));
        label_41 = new QLabel(tab);
        label_41->setObjectName(QString::fromUtf8("label_41"));
        label_41->setGeometry(QRect(220, 200, 121, 21));
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        layoutWidget = new QWidget(tab_2);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 0, 694, 534));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(layoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label_7->setFont(font);

        verticalLayout_2->addWidget(label_7);

        lblStatusEmail = new QLabel(layoutWidget);
        lblStatusEmail->setObjectName(QString::fromUtf8("lblStatusEmail"));
        lblStatusEmail->setFont(font);

        verticalLayout_2->addWidget(lblStatusEmail);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));

        verticalLayout_2->addLayout(horizontalLayout_6);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_4->addWidget(label_5);

        username = new QLineEdit(layoutWidget);
        username->setObjectName(QString::fromUtf8("username"));
        username->setMinimumSize(QSize(211, 27));
        username->setMaximumSize(QSize(211, 27));

        horizontalLayout_4->addWidget(username);

        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_4->addWidget(label_6);

        password = new QLineEdit(layoutWidget);
        password->setObjectName(QString::fromUtf8("password"));
        password->setEchoMode(QLineEdit::Password);

        horizontalLayout_4->addWidget(password);


        verticalLayout_2->addLayout(horizontalLayout_4);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(81, 0));
        label_8->setMaximumSize(QSize(81, 16777215));

        horizontalLayout_2->addWidget(label_8);

        sender = new QLineEdit(layoutWidget);
        sender->setObjectName(QString::fromUtf8("sender"));

        horizontalLayout_2->addWidget(sender);


        verticalLayout_2->addLayout(horizontalLayout_2);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_9 = new QLabel(layoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(81, 0));
        label_9->setMaximumSize(QSize(81, 16777215));

        horizontalLayout_5->addWidget(label_9);

        recipients = new QLineEdit(layoutWidget);
        recipients->setObjectName(QString::fromUtf8("recipients"));

        horizontalLayout_5->addWidget(recipients);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_10 = new QLabel(layoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(81, 0));
        label_10->setMaximumSize(QSize(81, 16777215));

        horizontalLayout_7->addWidget(label_10);

        subject = new QLineEdit(layoutWidget);
        subject->setObjectName(QString::fromUtf8("subject"));
        subject->setReadOnly(true);

        horizontalLayout_7->addWidget(subject);


        verticalLayout_2->addLayout(horizontalLayout_7);

        sendEmail = new QPushButton(layoutWidget);
        sendEmail->setObjectName(QString::fromUtf8("sendEmail"));

        verticalLayout_2->addWidget(sendEmail);

        texteditor = new QTextEdit(layoutWidget);
        texteditor->setObjectName(QString::fromUtf8("texteditor"));
        texteditor->setTextInteractionFlags(Qt::LinksAccessibleByKeyboard|Qt::LinksAccessibleByMouse|Qt::TextBrowserInteraction|Qt::TextEditable|Qt::TextEditorInteraction|Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        verticalLayout_2->addWidget(texteditor);

        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        webView = new QWebView(tab_3);
        webView->setObjectName(QString::fromUtf8("webView"));
        webView->setGeometry(QRect(10, 460, 181, 51));
        webView->setUrl(QUrl(QString::fromUtf8("about:blank")));
        pbStop = new QPushButton(tab_3);
        pbStop->setObjectName(QString::fromUtf8("pbStop"));
        pbStop->setGeometry(QRect(280, 280, 75, 23));
        pbStart = new QPushButton(tab_3);
        pbStart->setObjectName(QString::fromUtf8("pbStart"));
        pbStart->setGeometry(QRect(190, 280, 75, 23));
        lblKameraLabel = new QLabel(tab_3);
        lblKameraLabel->setObjectName(QString::fromUtf8("lblKameraLabel"));
        lblKameraLabel->setGeometry(QRect(10, 20, 351, 241));
        lblKameraLabel->setFrameShape(QFrame::Box);
        lblKameraLabel->setScaledContents(false);
        groupBox_8 = new QGroupBox(tab_3);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        groupBox_8->setGeometry(QRect(510, 60, 221, 131));
        txtIPCAM = new QTextEdit(groupBox_8);
        txtIPCAM->setObjectName(QString::fromUtf8("txtIPCAM"));
        txtIPCAM->setGeometry(QRect(-40, 100, 311, 31));
        label_20 = new QLabel(groupBox_8);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setGeometry(QRect(50, 80, 57, 20));
        QFont font1;
        font1.setPointSize(12);
        label_20->setFont(font1);
        btnReloadIPCAm = new QPushButton(groupBox_8);
        btnReloadIPCAm->setObjectName(QString::fromUtf8("btnReloadIPCAm"));
        btnReloadIPCAm->setGeometry(QRect(0, 50, 161, 25));
        tabWidget->addTab(tab_3, QString());
        groupBox_6 = new QGroupBox(centralWidget);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setGeometry(QRect(120, 0, 692, 15));
        layoutWidget1 = new QWidget(groupBox_6);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(0, 20, 692, 26));
        horizontalLayout = new QHBoxLayout(layoutWidget1);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget1);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(87, 17));
        label->setMaximumSize(QSize(87, 17));

        horizontalLayout->addWidget(label);

        host = new QLineEdit(layoutWidget1);
        host->setObjectName(QString::fromUtf8("host"));

        horizontalLayout->addWidget(host);

        label_2 = new QLabel(layoutWidget1);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(34, 17));
        label_2->setMaximumSize(QSize(34, 17));

        horizontalLayout->addWidget(label_2);

        port = new QSpinBox(layoutWidget1);
        port->setObjectName(QString::fromUtf8("port"));
        port->setMaximum(99999);
        port->setValue(465);

        horizontalLayout->addWidget(port);

        ssl = new QCheckBox(layoutWidget1);
        ssl->setObjectName(QString::fromUtf8("ssl"));
        ssl->setChecked(true);

        horizontalLayout->addWidget(ssl);

        layoutWidget2 = new QWidget(groupBox_6);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(0, 40, 149, 29));
        horizontalLayout_3 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(layoutWidget2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QFont font2;
        font2.setBold(false);
        font2.setWeight(50);
        label_4->setFont(font2);

        horizontalLayout_3->addWidget(label_4);

        auth = new QCheckBox(layoutWidget2);
        auth->setObjectName(QString::fromUtf8("auth"));
        auth->setMinimumSize(QSize(21, 21));
        auth->setMaximumSize(QSize(21, 21));
        auth->setChecked(true);
        auth->setTristate(false);

        horizontalLayout_3->addWidget(auth);

        layoutWidget_2 = new QWidget(groupBox_6);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(280, 10, 258, 99));
        verticalLayout = new QVBoxLayout(layoutWidget_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_3 = new QLabel(layoutWidget_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_8->addWidget(label_3);

        addAttachment = new QPushButton(layoutWidget_2);
        addAttachment->setObjectName(QString::fromUtf8("addAttachment"));
        addAttachment->setMinimumSize(QSize(97, 0));
        addAttachment->setMaximumSize(QSize(97, 16777215));

        horizontalLayout_8->addWidget(addAttachment);


        verticalLayout->addLayout(horizontalLayout_8);

        attachments = new QListWidget(layoutWidget_2);
        new QListWidgetItem(attachments);
        attachments->setObjectName(QString::fromUtf8("attachments"));
        attachments->setMaximumSize(QSize(16777215, 64));

        verticalLayout->addWidget(attachments);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 912, 19));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("MainWindow", "KAMERA", 0, QApplication::UnicodeUTF8));
        pbStartRecord->setText(QApplication::translate("MainWindow", "Start Record", 0, QApplication::UnicodeUTF8));
        pbStopRecord->setText(QApplication::translate("MainWindow", "Stop Record", 0, QApplication::UnicodeUTF8));
        label_21->setText(QApplication::translate("MainWindow", "status", 0, QApplication::UnicodeUTF8));
        lblStatusKamera->setText(QApplication::translate("MainWindow", "tidak merekam", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "STATUS INPUT", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("MainWindow", "PROXIMITY 1", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("MainWindow", "PROXIMITY 2", 0, QApplication::UnicodeUTF8));
        label_18->setText(QApplication::translate("MainWindow", "IJIN MASUK", 0, QApplication::UnicodeUTF8));
        label_19->setText(QApplication::translate("MainWindow", "RESET", 0, QApplication::UnicodeUTF8));
        lblVal1->setText(QApplication::translate("MainWindow", "nilai", 0, QApplication::UnicodeUTF8));
        lblVal2->setText(QApplication::translate("MainWindow", "nilai", 0, QApplication::UnicodeUTF8));
        groupBox_5->setTitle(QApplication::translate("MainWindow", "BACA INPUT", 0, QApplication::UnicodeUTF8));
        label_16->setText(QApplication::translate("MainWindow", "pin", 0, QApplication::UnicodeUTF8));
        pbReadInput->setText(QApplication::translate("MainWindow", "READ", 0, QApplication::UnicodeUTF8));
        label_17->setText(QApplication::translate("MainWindow", "status", 0, QApplication::UnicodeUTF8));
        lblVal3->setText(QApplication::translate("MainWindow", "nilai", 0, QApplication::UnicodeUTF8));
        lblVal4->setText(QApplication::translate("MainWindow", "nilai", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "OUTPUT", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("MainWindow", "PINTU", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("MainWindow", "JENDELA", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "OUTPUT", 0, QApplication::UnicodeUTF8));
        label_15->setText(QApplication::translate("MainWindow", "pin", 0, QApplication::UnicodeUTF8));
        pbOn->setText(QApplication::translate("MainWindow", "ON", 0, QApplication::UnicodeUTF8));
        pbOff->setText(QApplication::translate("MainWindow", "OFF", 0, QApplication::UnicodeUTF8));
        groupBox_7->setTitle(QApplication::translate("MainWindow", "OUTPUT", 0, QApplication::UnicodeUTF8));
        pbOn_2->setText(QApplication::translate("MainWindow", "BUZZER ON", 0, QApplication::UnicodeUTF8));
        pbOff_2->setText(QApplication::translate("MainWindow", "BUZZER OFF", 0, QApplication::UnicodeUTF8));
        pbOn_3->setText(QApplication::translate("MainWindow", "R1 ON", 0, QApplication::UnicodeUTF8));
        pbOff_3->setText(QApplication::translate("MainWindow", "R1 OFF", 0, QApplication::UnicodeUTF8));
        pbOn_4->setText(QApplication::translate("MainWindow", "R2 ON", 0, QApplication::UnicodeUTF8));
        pbOff_4->setText(QApplication::translate("MainWindow", "R2 OFF", 0, QApplication::UnicodeUTF8));
        pbOn_5->setText(QApplication::translate("MainWindow", "R3 ON", 0, QApplication::UnicodeUTF8));
        pbOff_5->setText(QApplication::translate("MainWindow", "R3 OFF", 0, QApplication::UnicodeUTF8));
        pbOn_6->setText(QApplication::translate("MainWindow", "R4 ON", 0, QApplication::UnicodeUTF8));
        pbOff_6->setText(QApplication::translate("MainWindow", "R4 OFF", 0, QApplication::UnicodeUTF8));
        pbOn_7->setText(QApplication::translate("MainWindow", "R5 ON", 0, QApplication::UnicodeUTF8));
        pbOff_7->setText(QApplication::translate("MainWindow", "R5 OFF", 0, QApplication::UnicodeUTF8));
        pbOn_8->setText(QApplication::translate("MainWindow", "R6 ON", 0, QApplication::UnicodeUTF8));
        pbOff_8->setText(QApplication::translate("MainWindow", "R6 OFF", 0, QApplication::UnicodeUTF8));
        lblstatusIjinMasuk->setText(QApplication::translate("MainWindow", "nilai", 0, QApplication::UnicodeUTF8));
        label_22->setText(QApplication::translate("MainWindow", "STATUS PENGAMAN", 0, QApplication::UnicodeUTF8));
        lblstatusTerkunci->setText(QApplication::translate("MainWindow", "NILAI", 0, QApplication::UnicodeUTF8));
        label_41->setText(QApplication::translate("MainWindow", "STATUS TERKUNCI=", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Tab 1", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("MainWindow", "DATA EMAIL", 0, QApplication::UnicodeUTF8));
        lblStatusEmail->setText(QApplication::translate("MainWindow", "last status email:", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "Username:", 0, QApplication::UnicodeUTF8));
        username->setText(QApplication::translate("MainWindow", "tugasakhirweb2015@gmail.com", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MainWindow", "Password:", 0, QApplication::UnicodeUTF8));
        password->setText(QApplication::translate("MainWindow", "jakartatimur92", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("MainWindow", "PENGIRIM", 0, QApplication::UnicodeUTF8));
        sender->setText(QApplication::translate("MainWindow", "tugasakhirweb2015@gmail.com", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("MainWindow", "PENERIMA", 0, QApplication::UnicodeUTF8));
        recipients->setText(QApplication::translate("MainWindow", "tugasakhirweb2015@gmail.com", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("MainWindow", "Subject:", 0, QApplication::UnicodeUTF8));
        subject->setText(QApplication::translate("MainWindow", "foto maling", 0, QApplication::UnicodeUTF8));
        sendEmail->setText(QApplication::translate("MainWindow", "Send Email", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "Tab 2", 0, QApplication::UnicodeUTF8));
        pbStop->setText(QApplication::translate("MainWindow", "stop", 0, QApplication::UnicodeUTF8));
        pbStart->setText(QApplication::translate("MainWindow", "start", 0, QApplication::UnicodeUTF8));
        lblKameraLabel->setText(QApplication::translate("MainWindow", "TextLabel", 0, QApplication::UnicodeUTF8));
        groupBox_8->setTitle(QApplication::translate("MainWindow", "GroupBox", 0, QApplication::UnicodeUTF8));
        txtIPCAM->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#008000;\">http://192.168.1.11:8080/jsfs.html</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        label_20->setText(QApplication::translate("MainWindow", "IPCAM", 0, QApplication::UnicodeUTF8));
        btnReloadIPCAm->setText(QApplication::translate("MainWindow", "Reload IP CAM", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("MainWindow", "Page", 0, QApplication::UnicodeUTF8));
        groupBox_6->setTitle(QApplication::translate("MainWindow", "GroupBox", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "SMTP server:", 0, QApplication::UnicodeUTF8));
        host->setText(QApplication::translate("MainWindow", "smtp.gmail.com", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Port:", 0, QApplication::UnicodeUTF8));
        ssl->setText(QApplication::translate("MainWindow", "SSL", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "Use authentication", 0, QApplication::UnicodeUTF8));
        auth->setText(QString());
        label_3->setText(QApplication::translate("MainWindow", "Attachments:", 0, QApplication::UnicodeUTF8));
        addAttachment->setText(QApplication::translate("MainWindow", "Add File", 0, QApplication::UnicodeUTF8));

        const bool __sortingEnabled = attachments->isSortingEnabled();
        attachments->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = attachments->item(0);
        ___qlistwidgetitem->setText(QApplication::translate("MainWindow", "/home/pi/Desktop/project/2016/TA/ISTN/sony/kamera/tes.jpg", 0, QApplication::UnicodeUTF8));
        attachments->setSortingEnabled(__sortingEnabled);

    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
