#include "classkamera.h"
#include <QDebug>
#include <QMutex>
#include <QTimer>
#include <QFile>
#include <QThread>

char key;
QMutex mutex;
QString namaFDGlobal2="ISTN";
QString alamatFdGlobal2="/media/pi/";
bool statusPerintahKamera;
ClassKamera::ClassKamera(QObject *parent) : QObject(parent)
{

    //qRegisterMetaType< cv::Mat >("Mat");
    statusPerintahKamera=false;
    connect(this,SIGNAL(signalRunCamera(QString)),this,SLOT(runKamera(QString)));
}
VideoWriter videoWriter;
VideoCapture cap;

void ClassKamera::runKamera(QString namaFile)
{
    //    while(1){

    //    Mat img0;
    //    cap >> img0;
    //    if(statusPerintahKamera && !img0.empty()){

    //        videoWriter << img0;
    //        //        imshow("image", img0);
    //        //char k = (char)waitKey(30);
    //        mutex.lock();
    //        emit resultCameraMath(img0);
    //        mutex.unlock();
    //}}
    //        QTimer::singleShot(0.1,this,SLOT(timerKamera()));
}


void ClassKamera::statusKamera(QString perintah)
{
    if(perintah=="start")
    {
        qDebug() <<"start kamera";
        statusPerintahKamera=true;

        cap.open(0);
        if (!cap.isOpened())
        {
            printf("can not open camera or video file\n");
            // return ;
        }
        QString alamatFdTemp=alamatFdGlobal2+namaFDGlobal2+"/videoku.avi";
        // string namemove("/media/ISTN/videoku.avi");
        string namemove (alamatFdTemp.toStdString());
        int fourCC = CV_FOURCC('M', 'J', 'P', 'G');
        //    int CAP_PROP_FRAME_WIDTH=320;
        //    int CAP_PROP_FRAME_HEIGHT=320;
        //    int CAP_PROP_FPS=15;
        //    Size S = Size((int)cap.get(CAP_PROP_FRAME_WIDTH), (int)cap.get(CAP_PROP_FRAME_HEIGHT));
        //    int fps = cap.get(CAP_PROP_FPS);
        //    videoWriter.open(namemove, -1, cap.get(CAP_PROP_FPS), S, true);
        double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
        double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

        cout << "Frame Size = " << dWidth << "x" << dHeight << endl;
        Size frameSize(static_cast<int>(dWidth), static_cast<int>(dHeight));

        //VideoWriter/oVideoWriter("MyVideo.avi", CV_FOURCC('M', 'J', 'P', 'G') , 20, frameSize, true); //initialize the VideoWriter object
        //  videoWriter.open(namemove, CV_FOURCC('M','S','V','C'), cap.get(15), frameSize, true);

        videoWriter.open(namemove,CV_FOURCC('M','J','P','G'),10, frameSize, true);

        if (!videoWriter.isOpened())
        {
            cerr << "Cannot open output file " << endl;

            return ;
        }
        //emit this->signalRunCamera("tes");
        QTimer::singleShot(1,this,SLOT(timerKamera()));
    }else if (perintah=="stop"){

        statusPerintahKamera=false;
    }
    qDebug()<< perintah;
}

void ClassKamera::timerKamera()
{
    Mat img0;
    cap >> img0;
    // cap.set(CV_CAP_PROP_FPS, 25);
    if(statusPerintahKamera && !img0.empty()){

        videoWriter << img0;
        //mutex.lock();
        emit resultCameraMath(img0);
        //mutex.unlock();
        QTimer::singleShot(1,this,SLOT(timerKamera()));

    }else{
        videoWriter.release();
        qDebug() <<"stop kamera";
        img0.release();
        cap.release();
        emit resultCameraMath(img0);

    }

}
