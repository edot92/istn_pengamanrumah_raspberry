
QT       += core gui network  webkit webkitwidgets
QT_CONFIG -= no-pkg-config
CONFIG += link_pkgconfig
PKGCONFIG += opencv
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = istn5_8_16
TEMPLATE = app
LIBS += -lwiringPi
LIBS += -lwiringPiDev
#LIBS += -L SmtpClient-for-Qt-1.1/release/ -lSMTPEmail
INCLUDEPATH += SmtpClient-for-Qt-1.1/src

SOURCES += main.cpp\
        mainwindow.cpp \
    SmtpClient-for-Qt-1.1/src/smtpclient.cpp \
    SmtpClient-for-Qt-1.1/src/quotedprintable.cpp \
    SmtpClient-for-Qt-1.1/src/mimetext.cpp \
    SmtpClient-for-Qt-1.1/src/mimepart.cpp \
    SmtpClient-for-Qt-1.1/src/mimemultipart.cpp \
    SmtpClient-for-Qt-1.1/src/mimemessage.cpp \
    SmtpClient-for-Qt-1.1/src/mimeinlinefile.cpp \
    SmtpClient-for-Qt-1.1/src/mimehtml.cpp \
    SmtpClient-for-Qt-1.1/src/mimefile.cpp \
    SmtpClient-for-Qt-1.1/src/mimecontentformatter.cpp \
    SmtpClient-for-Qt-1.1/src/mimeattachment.cpp \
    SmtpClient-for-Qt-1.1/src/emailaddress.cpp \
    classkamera.cpp

HEADERS  += mainwindow.h \
    SmtpClient-for-Qt-1.1/src/SmtpMime \
    SmtpClient-for-Qt-1.1/src/smtpexports.h \
    SmtpClient-for-Qt-1.1/src/smtpclient.h \
    SmtpClient-for-Qt-1.1/src/quotedprintable.h \
    SmtpClient-for-Qt-1.1/src/mimetext.h \
    SmtpClient-for-Qt-1.1/src/mimepart.h \
    SmtpClient-for-Qt-1.1/src/mimemultipart.h \
    SmtpClient-for-Qt-1.1/src/mimemessage.h \
    SmtpClient-for-Qt-1.1/src/mimeinlinefile.h \
    SmtpClient-for-Qt-1.1/src/mimehtml.h \
    SmtpClient-for-Qt-1.1/src/mimefile.h \
    SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
    SmtpClient-for-Qt-1.1/src/mimeattachment.h \
    SmtpClient-for-Qt-1.1/src/emailaddress.h \
    classkamera.h

FORMS    += mainwindow.ui

#OTHER_FILES += \
#    liblaryqt/SmtpClient-for-Qt-1.1/src/SMTPEmail.dll
