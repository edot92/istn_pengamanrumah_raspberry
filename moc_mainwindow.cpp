/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.6)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.6. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      29,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   30,   39,   39, 0x05,
      40,   62,   39,   39, 0x05,

 // slots: signature, parameters, type, tag, flags
      75,   39,   39,   39, 0x08,
     102,   39,   39,   39, 0x08,
     125,   39,   39,   39, 0x08,
     152,   39,   39,   39, 0x08,
     178,   39,   39,   39, 0x08,
     192,   39,   39,   39, 0x08,
     202,   39,   39,   39, 0x08,
     220,   39,   39,   39, 0x08,
     239,   39,   39,   39, 0x08,
     264,   39,   39,   39, 0x08,
     284,   39,   39,   39, 0x08,
     304,   39,   39,   39, 0x08,
     324,   39,   39,   39, 0x08,
     344,   39,   39,   39, 0x08,
     364,   39,   39,   39, 0x08,
     384,   39,   39,   39, 0x08,
     404,   39,   39,   39, 0x08,
     425,   39,   39,   39, 0x08,
     446,   39,   39,   39, 0x08,
     467,   39,   39,   39, 0x08,
     488,   39,   39,   39, 0x08,
     509,   39,   39,   39, 0x08,
     530,   39,   39,   39, 0x08,
     551,   39,   39,   39, 0x08,
     579,   39,   39,   39, 0x08,
     601,   39,   39,   39, 0x08,
     622,   39,   39,   39, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0runKamera(QString)\0namaFile\0"
    "\0statusKamera(QString)\0statusKamera\0"
    "on_addAttachment_clicked()\0"
    "on_sendEmail_clicked()\0"
    "on_pbStartRecord_clicked()\0"
    "on_pbStopRecord_clicked()\0updateTimer()\0"
    "rekaman()\0on_pbOn_clicked()\0"
    "on_pbOff_clicked()\0on_pbReadInput_clicked()\0"
    "on_pbOn_2_clicked()\0on_pbOn_3_clicked()\0"
    "on_pbOn_4_clicked()\0on_pbOn_5_clicked()\0"
    "on_pbOn_6_clicked()\0on_pbOn_7_clicked()\0"
    "on_pbOn_8_clicked()\0on_pbOff_2_clicked()\0"
    "on_pbOff_3_clicked()\0on_pbOff_4_clicked()\0"
    "on_pbOff_5_clicked()\0on_pbOff_6_clicked()\0"
    "on_pbOff_7_clicked()\0on_pbOff_8_clicked()\0"
    "on_btnReloadIPCAm_clicked()\0"
    "resultCameraMath(Mat)\0on_pbStart_clicked()\0"
    "on_pbStop_clicked()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->runKamera((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->statusKamera((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->on_addAttachment_clicked(); break;
        case 3: _t->on_sendEmail_clicked(); break;
        case 4: _t->on_pbStartRecord_clicked(); break;
        case 5: _t->on_pbStopRecord_clicked(); break;
        case 6: _t->updateTimer(); break;
        case 7: _t->rekaman(); break;
        case 8: _t->on_pbOn_clicked(); break;
        case 9: _t->on_pbOff_clicked(); break;
        case 10: _t->on_pbReadInput_clicked(); break;
        case 11: _t->on_pbOn_2_clicked(); break;
        case 12: _t->on_pbOn_3_clicked(); break;
        case 13: _t->on_pbOn_4_clicked(); break;
        case 14: _t->on_pbOn_5_clicked(); break;
        case 15: _t->on_pbOn_6_clicked(); break;
        case 16: _t->on_pbOn_7_clicked(); break;
        case 17: _t->on_pbOn_8_clicked(); break;
        case 18: _t->on_pbOff_2_clicked(); break;
        case 19: _t->on_pbOff_3_clicked(); break;
        case 20: _t->on_pbOff_4_clicked(); break;
        case 21: _t->on_pbOff_5_clicked(); break;
        case 22: _t->on_pbOff_6_clicked(); break;
        case 23: _t->on_pbOff_7_clicked(); break;
        case 24: _t->on_pbOff_8_clicked(); break;
        case 25: _t->on_btnReloadIPCAm_clicked(); break;
        case 26: _t->resultCameraMath((*reinterpret_cast< Mat(*)>(_a[1]))); break;
        case 27: _t->on_pbStart_clicked(); break;
        case 28: _t->on_pbStop_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 29)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 29;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::runKamera(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::statusKamera(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
