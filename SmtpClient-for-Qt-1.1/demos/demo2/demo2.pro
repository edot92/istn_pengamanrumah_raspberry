#-------------------------------------------------
#
# Project created by QtCreator 2014-10-30T22:48:32
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = demo2
TEMPLATE = app

SOURCES += \
    demo2.cpp \
    sendemail.cpp \
    ../../src/smtpclient.cpp \
    ../../src/quotedprintable.cpp \
    ../../src/mimetext.cpp \
    ../../src/mimepart.cpp \
    ../../src/mimemultipart.cpp \
    ../../src/mimemessage.cpp \
    ../../src/mimeinlinefile.cpp \
    ../../src/mimehtml.cpp \
    ../../src/mimefile.cpp \
    ../../src/mimecontentformatter.cpp \
    ../../src/mimeattachment.cpp \
    ../../src/emailaddress.cpp

# Location of SMTP Library
SMTP_LIBRARY_LOCATION = $$PWD/../../../build/SMTPEmail-Desktop-Debug

win32:CONFIG(release, debug|release): LIBS += -L$$SMTP_LIBRARY_LOCATION/release/ -lSMTPEmail
else:win32:CONFIG(debug, debug|release): LIBS += -L$$SMTP_LIBRARY_LOCATION/debug/ -lSMTPEmail
else:unix: LIBS += -L$$SMTP_LIBRARY_LOCATION -lSMTPEmail

INCLUDEPATH += $$SMTP_LIBRARY_LOCATION
DEPENDPATH += $$SMTP_LIBRARY_LOCATION
#LIBS += -LD:/edy/Latihan/QT/SmtpClient-for-Qt-1.1/release/ -lSMTPEmail
LIBS += -L/home/pi/liblaryqt/SmtpClient-for-Qt-1.1/release/ -lSMTPEmail
#LIBS += D:\edy\Latihan\QT\SmtpClient-for-Qt-1.1\release\
#LIBS += D:/edy/Latihan/QT/SmtpClient-for-Qt-1.1/release/
HEADERS += \
    sendemail.h \
    ../../src/SmtpMime \
    ../../src/smtpexports.h \
    ../../src/smtpclient.h \
    ../../src/quotedprintable.h \
    ../../src/mimetext.h \
    ../../src/mimepart.h \
    ../../src/mimemultipart.h \
    ../../src/mimemessage.h \
    ../../src/mimeinlinefile.h \
    ../../src/mimehtml.h \
    ../../src/mimefile.h \
    ../../src/mimecontentformatter.h \
    ../../src/mimeattachment.h \
    ../../src/emailaddress.h

FORMS += \
    sendemail.ui

OTHER_FILES += \
    ../../src/SMTPEmail.dll
