#-------------------------------------------------
#
# Project created by QtCreator 2014-10-30T22:19:03
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = demo1
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -LD:/edy/Latihan/QT/SmtpClient-for-Qt-1.1/release/ -lSMTPEmail

SOURCES += \
    demo1.cpp \
    ../../src/smtpclient.cpp \
    ../../src/quotedprintable.cpp \
    ../../src/mimetext.cpp \
    ../../src/mimepart.cpp \
    ../../src/mimemultipart.cpp \
    ../../src/mimemessage.cpp \
    ../../src/mimeinlinefile.cpp \
    ../../src/mimehtml.cpp \
    ../../src/mimefile.cpp \
    ../../src/mimecontentformatter.cpp \
    ../../src/mimeattachment.cpp \
    ../../src/emailaddress.cpp

# Location of SMTP Library
SMTP_LIBRARY_LOCATION = $$PWD/../../../build/SMTPEmail-Desktop-Debug

win32:CONFIG(release, debug|release): LIBS += -L$$SMTP_LIBRARY_LOCATION/release/ -lSMTPEmail
else:win32:CONFIG(debug, debug|release): LIBS += -L$$SMTP_LIBRARY_LOCATION/debug/ -lSMTPEmail
else:unix: LIBS += -L$$SMTP_LIBRARY_LOCATION -lSMTPEmail
LIBS += -L/home/pi/Desktop/latihanqt/SmtpClient-for-Qt-1.1/release/ -lSMTPEmail
INCLUDEPATH += $$SMTP_LIBRARY_LOCATION
DEPENDPATH += $$SMTP_LIBRARY_LOCATION

OTHER_FILES += \
    ../../src/SMTPEmail.dll

HEADERS += \
    ../../src/SmtpMime \
    ../../src/smtpexports.h \
    ../../src/smtpclient.h \
    ../../src/quotedprintable.h \
    ../../src/mimetext.h \
    ../../src/mimepart.h \
    ../../src/mimemultipart.h \
    ../../src/mimemessage.h \
    ../../src/mimeinlinefile.h \
    ../../src/mimehtml.h \
    ../../src/mimefile.h \
    ../../src/mimecontentformatter.h \
    ../../src/mimeattachment.h \
    ../../src/emailaddress.h
