#ifndef CLASSKAMERA_H
#define CLASSKAMERA_H

#include <QObject>
#include <QTimer>
#include<opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>


using namespace cv;
using namespace std;

class ClassKamera : public QObject
{
    Q_OBJECT
public:
    explicit ClassKamera(QObject *parent = 0);

signals:
    void resultCameraMath(Mat);
    void signalRunCamera(QString namaFile);

public slots:
    void runKamera(QString namaFile);
    void statusKamera(QString perintah);
    void timerKamera();
};

#endif // CLASSKAMERA_H
