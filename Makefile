#############################################################################
# Makefile for building: istn5_8_16
# Generated by qmake (2.01a) (Qt 4.8.6) on: Sat May 28 00:09:50 2016
# Project:  rev2.pro
# Template: app
# Command: /usr/lib/arm-linux-gnueabihf/qt4/bin/qmake -spec /usr/share/qt4/mkspecs/linux-g++ CONFIG+=debug CONFIG+=declarative_debug -o Makefile rev2.pro
#############################################################################

####### Compiler, tools and options

CC            = gcc
CXX           = g++
DEFINES       = -DQT_WEBKIT -DQT_WEBKIT_LIB -DQT_GUI_LIB -DQT_NETWORK_LIB -DQT_CORE_LIB -DQT_SHARED
CFLAGS        = -pipe -g -Wall -W -D_REENTRANT $(DEFINES)
CXXFLAGS      = -pipe -g -Wall -W -D_REENTRANT $(DEFINES)
INCPATH       = -I/usr/share/qt4/mkspecs/linux-g++ -I. -I/usr/include/qt4/QtCore -I/usr/include/qt4/QtNetwork -I/usr/include/qt4/QtGui -I/usr/include/qt4/QtWebKit -I/usr/include/qt4 -I../../liblaryqt/SmtpClient-for-Qt-1.1/src -I/usr/include/opencv -I. -I.
LINK          = g++
LFLAGS        = 
LIBS          = $(SUBLIBS)  -L/usr/lib/arm-linux-gnueabihf -lwiringPi -lwiringPiDev /usr/lib/arm-linux-gnueabihf/libopencv_calib3d.so -lopencv_calib3d /usr/lib/arm-linux-gnueabihf/libopencv_contrib.so -lopencv_contrib /usr/lib/arm-linux-gnueabihf/libopencv_core.so -lopencv_core /usr/lib/arm-linux-gnueabihf/libopencv_features2d.so -lopencv_features2d /usr/lib/arm-linux-gnueabihf/libopencv_flann.so -lopencv_flann /usr/lib/arm-linux-gnueabihf/libopencv_gpu.so -lopencv_gpu /usr/lib/arm-linux-gnueabihf/libopencv_highgui.so -lopencv_highgui /usr/lib/arm-linux-gnueabihf/libopencv_imgproc.so -lopencv_imgproc /usr/lib/arm-linux-gnueabihf/libopencv_legacy.so -lopencv_legacy /usr/lib/arm-linux-gnueabihf/libopencv_ml.so -lopencv_ml /usr/lib/arm-linux-gnueabihf/libopencv_objdetect.so -lopencv_objdetect /usr/lib/arm-linux-gnueabihf/libopencv_ocl.so -lopencv_ocl /usr/lib/arm-linux-gnueabihf/libopencv_photo.so -lopencv_photo /usr/lib/arm-linux-gnueabihf/libopencv_stitching.so -lopencv_stitching /usr/lib/arm-linux-gnueabihf/libopencv_superres.so -lopencv_superres /usr/lib/arm-linux-gnueabihf/libopencv_ts.so -lopencv_ts /usr/lib/arm-linux-gnueabihf/libopencv_video.so -lopencv_video /usr/lib/arm-linux-gnueabihf/libopencv_videostab.so -lopencv_videostab -lQtWebKit -lQtGui -lQtNetwork -lQtCore -lpthread 
AR            = ar cqs
RANLIB        = 
QMAKE         = /usr/lib/arm-linux-gnueabihf/qt4/bin/qmake
TAR           = tar -cf
COMPRESS      = gzip -9f
COPY          = cp -f
SED           = sed
COPY_FILE     = $(COPY)
COPY_DIR      = $(COPY) -r
STRIP         = strip
INSTALL_FILE  = install -m 644 -p
INSTALL_DIR   = $(COPY_DIR)
INSTALL_PROGRAM = install -m 755 -p
DEL_FILE      = rm -f
SYMLINK       = ln -f -s
DEL_DIR       = rmdir
MOVE          = mv -f
CHK_DIR_EXISTS= test -d
MKDIR         = mkdir -p

####### Output directory

OBJECTS_DIR   = ./

####### Files

SOURCES       = main.cpp \
		mainwindow.cpp \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpclient.cpp \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/quotedprintable.cpp \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimetext.cpp \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.cpp \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemultipart.cpp \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemessage.cpp \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeinlinefile.cpp \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimehtml.cpp \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimefile.cpp \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.cpp \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeattachment.cpp \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/emailaddress.cpp \
		classkamera.cpp moc_mainwindow.cpp \
		moc_smtpclient.cpp \
		moc_quotedprintable.cpp \
		moc_mimepart.cpp \
		moc_mimemultipart.cpp \
		moc_mimehtml.cpp \
		moc_mimefile.cpp \
		moc_mimecontentformatter.cpp \
		moc_mimeattachment.cpp \
		moc_emailaddress.cpp \
		moc_classkamera.cpp
OBJECTS       = main.o \
		mainwindow.o \
		smtpclient.o \
		quotedprintable.o \
		mimetext.o \
		mimepart.o \
		mimemultipart.o \
		mimemessage.o \
		mimeinlinefile.o \
		mimehtml.o \
		mimefile.o \
		mimecontentformatter.o \
		mimeattachment.o \
		emailaddress.o \
		classkamera.o \
		moc_mainwindow.o \
		moc_smtpclient.o \
		moc_quotedprintable.o \
		moc_mimepart.o \
		moc_mimemultipart.o \
		moc_mimehtml.o \
		moc_mimefile.o \
		moc_mimecontentformatter.o \
		moc_mimeattachment.o \
		moc_emailaddress.o \
		moc_classkamera.o
DIST          = /usr/share/qt4/mkspecs/common/unix.conf \
		/usr/share/qt4/mkspecs/common/linux.conf \
		/usr/share/qt4/mkspecs/common/gcc-base.conf \
		/usr/share/qt4/mkspecs/common/gcc-base-unix.conf \
		/usr/share/qt4/mkspecs/common/g++-base.conf \
		/usr/share/qt4/mkspecs/common/g++-unix.conf \
		/usr/share/qt4/mkspecs/qconfig.pri \
		/usr/share/qt4/mkspecs/modules/qt_webkit.pri \
		/usr/share/qt4/mkspecs/features/qt_functions.prf \
		/usr/share/qt4/mkspecs/features/qt_config.prf \
		/usr/share/qt4/mkspecs/features/exclusive_builds.prf \
		/usr/share/qt4/mkspecs/features/default_pre.prf \
		/usr/share/qt4/mkspecs/features/debug.prf \
		/usr/share/qt4/mkspecs/features/default_post.prf \
		/usr/share/qt4/mkspecs/features/link_pkgconfig.prf \
		/usr/share/qt4/mkspecs/features/declarative_debug.prf \
		/usr/share/qt4/mkspecs/features/shared.prf \
		/usr/share/qt4/mkspecs/features/unix/gdb_dwarf_index.prf \
		/usr/share/qt4/mkspecs/features/warn_on.prf \
		/usr/share/qt4/mkspecs/features/qt.prf \
		/usr/share/qt4/mkspecs/features/unix/thread.prf \
		/usr/share/qt4/mkspecs/features/moc.prf \
		/usr/share/qt4/mkspecs/features/resources.prf \
		/usr/share/qt4/mkspecs/features/uic.prf \
		/usr/share/qt4/mkspecs/features/yacc.prf \
		/usr/share/qt4/mkspecs/features/lex.prf \
		/usr/share/qt4/mkspecs/features/include_source_dir.prf \
		rev2.pro
QMAKE_TARGET  = istn5_8_16
DESTDIR       = 
TARGET        = istn5_8_16

first: all
####### Implicit rules

.SUFFIXES: .o .c .cpp .cc .cxx .C

.cpp.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cc.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cxx.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.C.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.c.o:
	$(CC) -c $(CFLAGS) $(INCPATH) -o "$@" "$<"

####### Build rules

all: Makefile $(TARGET)

$(TARGET): ui_mainwindow.h $(OBJECTS)  
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(OBJCOMP) $(LIBS)
	{ test -n "$(DESTDIR)" && DESTDIR="$(DESTDIR)" || DESTDIR=.; } && test $$(gdb --version | sed -e 's,[^0-9][^0-9]*\([0-9]\)\.\([0-9]\).*,\1\2,;q') -gt 72 && gdb --nx --batch --quiet -ex 'set confirm off' -ex "save gdb-index $$DESTDIR" -ex quit '$(TARGET)' && test -f $(TARGET).gdb-index && objcopy --add-section '.gdb_index=$(TARGET).gdb-index' --set-section-flags '.gdb_index=readonly' '$(TARGET)' '$(TARGET)' && rm -f $(TARGET).gdb-index || true

Makefile: rev2.pro  /usr/share/qt4/mkspecs/linux-g++/qmake.conf /usr/share/qt4/mkspecs/common/unix.conf \
		/usr/share/qt4/mkspecs/common/linux.conf \
		/usr/share/qt4/mkspecs/common/gcc-base.conf \
		/usr/share/qt4/mkspecs/common/gcc-base-unix.conf \
		/usr/share/qt4/mkspecs/common/g++-base.conf \
		/usr/share/qt4/mkspecs/common/g++-unix.conf \
		/usr/share/qt4/mkspecs/qconfig.pri \
		/usr/share/qt4/mkspecs/modules/qt_webkit.pri \
		/usr/share/qt4/mkspecs/features/qt_functions.prf \
		/usr/share/qt4/mkspecs/features/qt_config.prf \
		/usr/share/qt4/mkspecs/features/exclusive_builds.prf \
		/usr/share/qt4/mkspecs/features/default_pre.prf \
		/usr/share/qt4/mkspecs/features/debug.prf \
		/usr/share/qt4/mkspecs/features/default_post.prf \
		/usr/share/qt4/mkspecs/features/link_pkgconfig.prf \
		/usr/share/qt4/mkspecs/features/declarative_debug.prf \
		/usr/share/qt4/mkspecs/features/shared.prf \
		/usr/share/qt4/mkspecs/features/unix/gdb_dwarf_index.prf \
		/usr/share/qt4/mkspecs/features/warn_on.prf \
		/usr/share/qt4/mkspecs/features/qt.prf \
		/usr/share/qt4/mkspecs/features/unix/thread.prf \
		/usr/share/qt4/mkspecs/features/moc.prf \
		/usr/share/qt4/mkspecs/features/resources.prf \
		/usr/share/qt4/mkspecs/features/uic.prf \
		/usr/share/qt4/mkspecs/features/yacc.prf \
		/usr/share/qt4/mkspecs/features/lex.prf \
		/usr/share/qt4/mkspecs/features/include_source_dir.prf \
		/usr/lib/arm-linux-gnueabihf/libQtWebKit.prl \
		/usr/lib/arm-linux-gnueabihf/libQtGui.prl \
		/usr/lib/arm-linux-gnueabihf/libQtNetwork.prl \
		/usr/lib/arm-linux-gnueabihf/libQtCore.prl
	$(QMAKE) -spec /usr/share/qt4/mkspecs/linux-g++ CONFIG+=debug CONFIG+=declarative_debug -o Makefile rev2.pro
/usr/share/qt4/mkspecs/common/unix.conf:
/usr/share/qt4/mkspecs/common/linux.conf:
/usr/share/qt4/mkspecs/common/gcc-base.conf:
/usr/share/qt4/mkspecs/common/gcc-base-unix.conf:
/usr/share/qt4/mkspecs/common/g++-base.conf:
/usr/share/qt4/mkspecs/common/g++-unix.conf:
/usr/share/qt4/mkspecs/qconfig.pri:
/usr/share/qt4/mkspecs/modules/qt_webkit.pri:
/usr/share/qt4/mkspecs/features/qt_functions.prf:
/usr/share/qt4/mkspecs/features/qt_config.prf:
/usr/share/qt4/mkspecs/features/exclusive_builds.prf:
/usr/share/qt4/mkspecs/features/default_pre.prf:
/usr/share/qt4/mkspecs/features/debug.prf:
/usr/share/qt4/mkspecs/features/default_post.prf:
/usr/share/qt4/mkspecs/features/link_pkgconfig.prf:
/usr/share/qt4/mkspecs/features/declarative_debug.prf:
/usr/share/qt4/mkspecs/features/shared.prf:
/usr/share/qt4/mkspecs/features/unix/gdb_dwarf_index.prf:
/usr/share/qt4/mkspecs/features/warn_on.prf:
/usr/share/qt4/mkspecs/features/qt.prf:
/usr/share/qt4/mkspecs/features/unix/thread.prf:
/usr/share/qt4/mkspecs/features/moc.prf:
/usr/share/qt4/mkspecs/features/resources.prf:
/usr/share/qt4/mkspecs/features/uic.prf:
/usr/share/qt4/mkspecs/features/yacc.prf:
/usr/share/qt4/mkspecs/features/lex.prf:
/usr/share/qt4/mkspecs/features/include_source_dir.prf:
/usr/lib/arm-linux-gnueabihf/libQtWebKit.prl:
/usr/lib/arm-linux-gnueabihf/libQtGui.prl:
/usr/lib/arm-linux-gnueabihf/libQtNetwork.prl:
/usr/lib/arm-linux-gnueabihf/libQtCore.prl:
qmake:  FORCE
	@$(QMAKE) -spec /usr/share/qt4/mkspecs/linux-g++ CONFIG+=debug CONFIG+=declarative_debug -o Makefile rev2.pro

dist: 
	@$(CHK_DIR_EXISTS) .tmp/istn5_8_161.0.0 || $(MKDIR) .tmp/istn5_8_161.0.0 
	$(COPY_FILE) --parents $(SOURCES) $(DIST) .tmp/istn5_8_161.0.0/ && $(COPY_FILE) --parents mainwindow.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/SmtpMime ../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpclient.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/quotedprintable.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimetext.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemultipart.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemessage.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeinlinefile.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimehtml.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimefile.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeattachment.h ../../liblaryqt/SmtpClient-for-Qt-1.1/src/emailaddress.h classkamera.h .tmp/istn5_8_161.0.0/ && $(COPY_FILE) --parents main.cpp mainwindow.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpclient.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/quotedprintable.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimetext.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemultipart.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemessage.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeinlinefile.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimehtml.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimefile.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeattachment.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/emailaddress.cpp classkamera.cpp .tmp/istn5_8_161.0.0/ && $(COPY_FILE) --parents mainwindow.ui .tmp/istn5_8_161.0.0/ && (cd `dirname .tmp/istn5_8_161.0.0` && $(TAR) istn5_8_161.0.0.tar istn5_8_161.0.0 && $(COMPRESS) istn5_8_161.0.0.tar) && $(MOVE) `dirname .tmp/istn5_8_161.0.0`/istn5_8_161.0.0.tar.gz . && $(DEL_FILE) -r .tmp/istn5_8_161.0.0


clean:compiler_clean 
	-$(DEL_FILE) $(OBJECTS)
	-$(DEL_FILE) *~ core *.core


####### Sub-libraries

distclean: clean
	-$(DEL_FILE) $(TARGET) 
	-$(DEL_FILE) Makefile


check: first

mocclean: compiler_moc_header_clean compiler_moc_source_clean

mocables: compiler_moc_header_make_all compiler_moc_source_make_all

compiler_moc_header_make_all: moc_mainwindow.cpp moc_smtpclient.cpp moc_quotedprintable.cpp moc_mimepart.cpp moc_mimemultipart.cpp moc_mimehtml.cpp moc_mimefile.cpp moc_mimecontentformatter.cpp moc_mimeattachment.cpp moc_emailaddress.cpp moc_classkamera.cpp
compiler_moc_header_clean:
	-$(DEL_FILE) moc_mainwindow.cpp moc_smtpclient.cpp moc_quotedprintable.cpp moc_mimepart.cpp moc_mimemultipart.cpp moc_mimehtml.cpp moc_mimefile.cpp moc_mimecontentformatter.cpp moc_mimeattachment.cpp moc_emailaddress.cpp moc_classkamera.cpp
moc_mainwindow.cpp: mainwindow.h
	/usr/lib/arm-linux-gnueabihf/qt4/bin/moc $(DEFINES) $(INCPATH) mainwindow.h -o moc_mainwindow.cpp

moc_smtpclient.cpp: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemessage.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemultipart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/emailaddress.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpclient.h
	/usr/lib/arm-linux-gnueabihf/qt4/bin/moc $(DEFINES) $(INCPATH) ../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpclient.h -o moc_smtpclient.cpp

moc_quotedprintable.cpp: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/quotedprintable.h
	/usr/lib/arm-linux-gnueabihf/qt4/bin/moc $(DEFINES) $(INCPATH) ../../liblaryqt/SmtpClient-for-Qt-1.1/src/quotedprintable.h -o moc_quotedprintable.cpp

moc_mimepart.cpp: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h
	/usr/lib/arm-linux-gnueabihf/qt4/bin/moc $(DEFINES) $(INCPATH) ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h -o moc_mimepart.cpp

moc_mimemultipart.cpp: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemultipart.h
	/usr/lib/arm-linux-gnueabihf/qt4/bin/moc $(DEFINES) $(INCPATH) ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemultipart.h -o moc_mimemultipart.cpp

moc_mimehtml.cpp: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimetext.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimehtml.h
	/usr/lib/arm-linux-gnueabihf/qt4/bin/moc $(DEFINES) $(INCPATH) ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimehtml.h -o moc_mimehtml.cpp

moc_mimefile.cpp: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimefile.h
	/usr/lib/arm-linux-gnueabihf/qt4/bin/moc $(DEFINES) $(INCPATH) ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimefile.h -o moc_mimefile.cpp

moc_mimecontentformatter.cpp: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h
	/usr/lib/arm-linux-gnueabihf/qt4/bin/moc $(DEFINES) $(INCPATH) ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h -o moc_mimecontentformatter.cpp

moc_mimeattachment.cpp: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimefile.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeattachment.h
	/usr/lib/arm-linux-gnueabihf/qt4/bin/moc $(DEFINES) $(INCPATH) ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeattachment.h -o moc_mimeattachment.cpp

moc_emailaddress.cpp: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/emailaddress.h
	/usr/lib/arm-linux-gnueabihf/qt4/bin/moc $(DEFINES) $(INCPATH) ../../liblaryqt/SmtpClient-for-Qt-1.1/src/emailaddress.h -o moc_emailaddress.cpp

moc_classkamera.cpp: classkamera.h
	/usr/lib/arm-linux-gnueabihf/qt4/bin/moc $(DEFINES) $(INCPATH) classkamera.h -o moc_classkamera.cpp

compiler_rcc_make_all:
compiler_rcc_clean:
compiler_image_collection_make_all: qmake_image_collection.cpp
compiler_image_collection_clean:
	-$(DEL_FILE) qmake_image_collection.cpp
compiler_moc_source_make_all:
compiler_moc_source_clean:
compiler_uic_make_all: ui_mainwindow.h
compiler_uic_clean:
	-$(DEL_FILE) ui_mainwindow.h
ui_mainwindow.h: mainwindow.ui
	/usr/lib/arm-linux-gnueabihf/qt4/bin/uic mainwindow.ui -o ui_mainwindow.h

compiler_yacc_decl_make_all:
compiler_yacc_decl_clean:
compiler_yacc_impl_make_all:
compiler_yacc_impl_clean:
compiler_lex_make_all:
compiler_lex_clean:
compiler_clean: compiler_moc_header_clean compiler_uic_clean 

####### Compile

main.o: main.cpp mainwindow.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o main.o main.cpp

mainwindow.o: mainwindow.cpp mainwindow.h \
		ui_mainwindow.h \
		classkamera.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o mainwindow.o mainwindow.cpp

smtpclient.o: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpclient.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpclient.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemessage.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemultipart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/emailaddress.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o smtpclient.o ../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpclient.cpp

quotedprintable.o: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/quotedprintable.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/quotedprintable.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o quotedprintable.o ../../liblaryqt/SmtpClient-for-Qt-1.1/src/quotedprintable.cpp

mimetext.o: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimetext.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimetext.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o mimetext.o ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimetext.cpp

mimepart.o: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/quotedprintable.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o mimepart.o ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.cpp

mimemultipart.o: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemultipart.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemultipart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o mimemultipart.o ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemultipart.cpp

mimemessage.o: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemessage.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemessage.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemultipart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/emailaddress.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/quotedprintable.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o mimemessage.o ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimemessage.cpp

mimeinlinefile.o: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeinlinefile.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeinlinefile.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimefile.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o mimeinlinefile.o ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeinlinefile.cpp

mimehtml.o: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimehtml.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimehtml.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimetext.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o mimehtml.o ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimehtml.cpp

mimefile.o: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimefile.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimefile.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o mimefile.o ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimefile.cpp

mimecontentformatter.o: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o mimecontentformatter.o ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.cpp

mimeattachment.o: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeattachment.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeattachment.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimepart.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimecontentformatter.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimefile.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o mimeattachment.o ../../liblaryqt/SmtpClient-for-Qt-1.1/src/mimeattachment.cpp

emailaddress.o: ../../liblaryqt/SmtpClient-for-Qt-1.1/src/emailaddress.cpp ../../liblaryqt/SmtpClient-for-Qt-1.1/src/emailaddress.h \
		../../liblaryqt/SmtpClient-for-Qt-1.1/src/smtpexports.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o emailaddress.o ../../liblaryqt/SmtpClient-for-Qt-1.1/src/emailaddress.cpp

classkamera.o: classkamera.cpp classkamera.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o classkamera.o classkamera.cpp

moc_mainwindow.o: moc_mainwindow.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_mainwindow.o moc_mainwindow.cpp

moc_smtpclient.o: moc_smtpclient.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_smtpclient.o moc_smtpclient.cpp

moc_quotedprintable.o: moc_quotedprintable.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_quotedprintable.o moc_quotedprintable.cpp

moc_mimepart.o: moc_mimepart.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_mimepart.o moc_mimepart.cpp

moc_mimemultipart.o: moc_mimemultipart.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_mimemultipart.o moc_mimemultipart.cpp

moc_mimehtml.o: moc_mimehtml.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_mimehtml.o moc_mimehtml.cpp

moc_mimefile.o: moc_mimefile.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_mimefile.o moc_mimefile.cpp

moc_mimecontentformatter.o: moc_mimecontentformatter.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_mimecontentformatter.o moc_mimecontentformatter.cpp

moc_mimeattachment.o: moc_mimeattachment.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_mimeattachment.o moc_mimeattachment.cpp

moc_emailaddress.o: moc_emailaddress.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_emailaddress.o moc_emailaddress.cpp

moc_classkamera.o: moc_classkamera.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_classkamera.o moc_classkamera.cpp

####### Install

install:   FORCE

uninstall:   FORCE

FORCE:

