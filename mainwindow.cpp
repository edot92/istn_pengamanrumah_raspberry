#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QErrorMessage>
#include <QMessageBox>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv.h>
#include <highgui.h>
#include "opencv2/opencv.hpp"
#include <wiringPi.h>
#include <QTimer>
#include <QFile>
#include <QDir>
#include <QWebView>
#include <QUrl>
#include <QDateTime>
#include <QThread>
#include <QTimer>
#include <classkamera.h>
#define R1 1
#define R2 4
#define R3 5
#define R4 6
#define R5 10
#define R6 11
#define R7 26
#define btn1 0
#define btn2 2
#define btn3 3
#define btn4 12


#define buka 1
#define tutup 2
#define delayMotor 950
QString stripKamera;
int sensorIjinMasuk = btn4;
int sensorReset = btn3;

using namespace std;
int xx;
using namespace cv;
//int pinout[12]={1,4,5,6,10,11,31,26,27,28,29};
int pinout[7]={1,4,5,6,10,11,26};

//int pinout[14]={7,0,2,3,12,13,14,30,21,22,23,24,25};
bool flagRekam;
QString namaFDGlobal="ISTN";
QString alamatFdGlobal="/media/pi/";
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    stripKamera="";
    QFile file("datatxt/ipkamera.txt");
    if(file.open(QIODevice::ReadWrite)){
        QByteArray a=file.readAll();
        if(a.isEmpty()){
            const  char *aa="http://192.168.1.11:8080/jsfs.html";
            file.write(aa);
        }else{
            QString cc="";
            for(int x=0;x<a.count();x++){
                cc=cc+QString(a.at(x));
            }
            stripKamera=cc;
            ui->txtIPCAM->setText(cc);
        }

        file.close();
    }
    file.setFileName("datatxt/usernameemail.txt");
    if(file.open(QIODevice::ReadWrite)){
        QByteArray a=file.readAll();
        if(a.isEmpty()){
            const  char *aa="tugasakhirweb2015@gmail.com";
            file.write(aa);
        }else{
            QString cc="";
            for(int x=0;x<a.count();x++){
                cc=cc+QString(a.at(x));
            }
            ui->username->clear();
            ui->username->setText(cc);

        }

        file.close();
    }
    file.setFileName("datatxt/passemail.txt");
    if(file.open(QIODevice::ReadWrite)){
        QByteArray a=file.readAll();
        if(a.isEmpty()){
            const  char *aa="jakartatimur92";
            file.write(aa);
        }else{
            QString cc="";
            for(int x=0;x<a.count();x++){
                cc=cc+QString(a.at(x));
            }
            ui->password->clear();
            ui->password->setText(cc);

        }

        file.close();
    }
    file.setFileName("datatxt/senderemail.txt");
    if(file.open(QIODevice::ReadWrite)){
        QByteArray a=file.readAll();
        if(a.isEmpty()){
            const  char *aa="tugasakhirweb2015@gmail.com";
            file.write(aa);
        }else{
            QString cc="";
            for(int x=0;x<a.count();x++){
                cc=cc+QString(a.at(x));
            }
            ui->sender->clear();
            ui->sender->setText(cc);

        }

        file.close();
    }
//    file.setFileName("/media/pi/ISTN/recipients.txt");
//    if(file.open(QIODevice::ReadWrite)){
//        QByteArray a=file.readAll();
//        if(a.isEmpty()){
//            const  char *aa="tugasakhirweb2015@gmail.com";
//            file.write(aa);
//        }else{
//            QString cc="";

//            cc.clear();
//            a.replace("\r","");
//            a.replace("\n","");
//            a.replace(" ","");

//            for(int x=0;x<a.count();x++){
//                cc=cc+QString(a.at(x));
//            }
//            ui->recipients->clear();
//            ui->recipients->setText(cc);

//        }

//        file.close();
//    }
    xx=0;
    ui->groupBox_6->setVisible(false);
    ui->sender->setVisible(false);
    ui->label_8->setVisible(false);
    connect(ui->pbStartRecord,SIGNAL(clicked()),
            SLOT(rekaman()));
    timer1= new QTimer(this );
    //    connect(timer1,SIGNAL(timeout()),this,SLOT(updateTimer()));
    //    timer1->start(1000);


    wiringPiSetup();
    for(int z=0;z<7;z++){
        qDebug()<< QString::number(z);
        pinMode(pinout[z],OUTPUT);

        //  digitalWrite(5,HIGH);
        digitalWrite(pinout[z],HIGH);

        delay(50);
        //  digitalWrite(5,LOW);
        digitalWrite(pinout[z],LOW);
        delay(50);
    }

    motorjendela(true);
    //  motorpintu(true);
    ijinBuka=false;
    terKunci=false;
    ui->lblstatusIjinMasuk->setText(" aktif");
    QTimer::singleShot(200,this,SLOT(updateTimer()));
    qRegisterMetaType< cv::Mat >("Mat");

    QThread *th1;
    ClassKamera *classkamera;
    th1= new QThread();
    classkamera = new ClassKamera();
    classkamera->moveToThread(th1);

    connect(classkamera,SIGNAL(resultCameraMath(Mat)),this,SLOT(resultCameraMath(Mat)));
    connect(this,SIGNAL(runKamera(QString)),classkamera,SLOT(runKamera(QString)));
    connect(this,SIGNAL(runKamera(QString)),classkamera,SLOT(runKamera(QString)));
    connect(this,SIGNAL(statusKamera(QString)),classkamera,SLOT(statusKamera(QString)));
    th1->start();

    ui->groupBox_8->setVisible(false);

}

void MainWindow::resultCameraMath(Mat image)
{ Mat dest;
    cvtColor(image, dest,CV_BGR2RGB);
    QImage image1= QImage((uchar*) dest.data, dest.cols, dest.rows, dest.step, QImage::Format_RGB888);

    //show Qimage using QLabel
    ui->lblKameraLabel->setPixmap(QPixmap::fromImage(image1));
    ui->lblKameraLabel->setScaledContents( true );
    ui->lblKameraLabel->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
    //ui->lblKameraLabel->show();
}

void MainWindow::updateTimer(){
    QString namaFD=namaFDGlobal;
    int xx=0;
jj:
    xx++;
    if(xx>1){
        namaFD=namaFD+"_";
    }

    QFile file(alamatFdGlobal+namaFD+"/recipients.txt");
    //qDebug()<< namaFD;
    if(file.open(QIODevice::ReadWrite)){
        QByteArray a=file.readAll();
        if(a.isEmpty()){
            //      const  char *aa="tugasakhirweb2015@gmail.com";
            //        file.write(aa);
            file.close();
            //       goto jj;
        }else{
            QString cc="";

            cc.clear();
            a.replace("\r","");
            a.replace("\n","");
            a.replace(" ","");

            for(int x=0;x<a.count();x++){
                cc=cc+QString(a.at(x));
            }
            ui->recipients->clear();
            ui->recipients->setText(cc);

        }
        file.close();
    }
    else{
        file.close();
        //     goto jj;
    }


    pinMode(ui->txtPin_INPUT->toPlainText().toInt(),INPUT);

    int a= digitalRead(ui->txtPin_INPUT->toPlainText().toInt());
    ui->txtStatusInput->setText(QString::number(a));
    a=digitalRead(btn1);
    ui->lblVal1->setText(QString::number(a));
    a=digitalRead(btn2);
    ui->lblVal2->setText(QString::number(a));
    int b =digitalRead(sensorIjinMasuk);
    ui->lblVal3->setText(QString::number(b));
    int c=digitalRead(sensorReset);
    ui->lblVal4->setText(QString::number(c));
    if(b==1 && ijinBuka==false && terKunci==false){ //sensor ijin masuk terkena objek
        for(int x=0;x<3;x++){
            digitalWrite(R1,HIGH);
            delay(200);
            digitalWrite(R1,LOW);
            delay(200);
        }
        ijinBuka=true;
        ui->lblstatusIjinMasuk->setText("Tidak aktif");
         ui->lblstatusTerkunci->setText("tidak terkunci");
        QTimer::singleShot(1000,this,SLOT(updateTimer()));
    }else   if(b==1 && ijinBuka==true && terKunci==false){ // aktifkan pengaman

        for(int x=0;x<3;x++){
            digitalWrite(R1,HIGH);
            delay(200);
            digitalWrite(R1,LOW);
            delay(1000);
        }
        ijinBuka=false;
        ui->lblstatusIjinMasuk->setText(" aktif");
        QTimer::singleShot(1000,this,SLOT(updateTimer()));
    }else{
        modeOtomatis();
        if (terKunci==true) {
            ui->lblstatusTerkunci->setText("terkunci");
            digitalWrite(R1,HIGH);
            delay(200);
            digitalWrite(R1,LOW);
            delay(200);
            QDateTime t;
            QString dd=t.currentDateTime().toString("ss");

            if(dd=="59"||dd=="58" )            {
                QDateTime dd;

                QString q="Foto Maling, Waktu :"+dd.currentDateTime().toString("dd-MM-yyyy hh:mm:ss");
                ui->subject->setText(q);
                on_sendEmail_clicked();

            }
        }
        QTimer::singleShot(200,this,SLOT(updateTimer()));
    }

}
void MainWindow::motorjendela(bool butuh_alat_call_082210160003){
    if(butuh_alat_call_082210160003){
        digitalWrite(R4,HIGH);
        digitalWrite(R5,LOW);
        digitalWrite(R2,HIGH);
        digitalWrite(R3,LOW);
        delay(delayMotor);
        digitalWrite(R5,LOW);
        digitalWrite(R4,LOW);
        digitalWrite(R2,LOW);
        digitalWrite(R3,LOW);
    }else{
        digitalWrite(R5,HIGH);
        digitalWrite(R4,LOW);
        digitalWrite(R2,LOW);
        digitalWrite(R3,HIGH);
        delay(delayMotor);
        digitalWrite(R3,LOW);
        digitalWrite(R4,LOW);
        digitalWrite(R2,LOW);
        digitalWrite(R5,LOW);
    }
}
void MainWindow::motorpintu(bool butuh_alat_call_082210160003){
    if(butuh_alat_call_082210160003){
        digitalWrite(R2,HIGH);
        digitalWrite(R3,LOW);
        delay(delayMotor);
        digitalWrite(R2,LOW);
        digitalWrite(R3,LOW);

    }else{
        digitalWrite(R3,HIGH);
        digitalWrite(R2,LOW);
        delay(delayMotor);
        digitalWrite(R2,LOW);
        digitalWrite(R3,LOW);
    }
}
MainWindow::~MainWindow()
{
    delete ui;
}

EmailAddress* MainWindow::stringToEmail(const QString &str)
{
    int p1 = str.indexOf("<");
    int p2 = str.indexOf(">");

    if (p1 == -1)
    {
        // no name, only email address
        return new EmailAddress(str);
    }
    else
    {
        return new EmailAddress(str.mid(p1 + 1, p2 - p1 - 1), str.left(p1));
    }

}

void MainWindow::on_addAttachment_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFiles);


    if (dialog.exec())
        ui->attachments->addItems(dialog.selectedFiles());


}

void MainWindow::on_sendEmail_clicked()
{
    QString host = ui->host->text();
    int port = ui->port->value();
    bool ssl = ui->ssl->isChecked();
    bool auth = ui->auth->isChecked();
    QString user = ui->username->text();
    QString password = ui->password->text();

    EmailAddress *sender = stringToEmail(ui->sender->text());

    QStringList rcptStringList = ui->recipients->text().split(';');

    QString subject = ui->subject->text();
    QString html = ui->texteditor->toHtml();

    SmtpClient smtp (host, port, ssl ? SmtpClient::SslConnection : SmtpClient::TcpConnection);

    MimeMessage message;

    message.setSender(sender);
    message.setSubject(subject);

    for (int i = 0; i < rcptStringList.size(); ++i)
        message.addRecipient(stringToEmail(rcptStringList.at(i)));

    MimeHtml content;
    content.setHtml(html);

    message.addPart(&content);

    for (int i = 0; i < ui->attachments->count(); ++i)
    {
        message.addPart(new MimeAttachment(new QFile(ui->attachments->item(i)->text())));
    }

    if (!smtp.connectToHost())
    {
        errorMessage("Connection Failed");
        return;
    }

    if (auth)
        if (!smtp.login(user, password))
        {
            errorMessage("Authentification Failed");
            return;
        }

    if (!smtp.sendMail(message))
    {
        errorMessage("Mail sending failed");
        return;
    }
    else
    {
        //        QMessageBox okMessage (this);
        //        okMessage.setText("The email was succesfully sent.");
        //        okMessage.exec();
    }

    smtp.quit();

    //    QFile file;
    //    file.setFileName("datatxt/usernameemail.txt");
    //    if(file.open(QIODevice::ReadWrite)){
    //        QString cc=ui->username->text();
    //        QByteArray dd;
    //        cc.replace("\r","");
    //        cc.replace("\n","");

    //        for(int x=0;x<cc.length();x++){
    //            dd.append(cc.at(x));
    //        }
    //        file.write(dd);
    //        stripKamera=cc;
    //        file.close();
    //    }
    //    file.setFileName("datatxt/passemail.txt");
    //    if(file.open(QIODevice::ReadWrite)){
    //        QString cc=ui->password->text();
    //        QByteArray dd;
    //        for(int x=0;x<cc.length();x++){
    //            dd.append(cc.at(x));
    //        }
    //        cc.replace("\r","");
    //        cc.replace("\n","");

    //        file.write(dd);
    //        stripKamera=cc;
    //        file.close();
    //    }
    //    file.setFileName("datatxt/senderemail.txt");
    //    if(file.open(QIODevice::ReadWrite)){


    //        QString cc=ui->sender->text();
    //        QByteArray dd;
    //        cc.replace("\r","");
    //        cc.replace("\n","");

    //        for(int x=0;x<cc.length();x++){
    //            dd.append(cc.at(x));
    //        }
    //        file.write(dd);
    //        stripKamera=cc;
    //        file.close();
    //    }
    //    file.setFileName("/media/ISTN/recipients.txt");
    //    if(file.open(QIODevice::ReadWrite)){
    //        QByteArray datRead=file.readAll();
    //        char tempChr[datRead.count()+1];
    //        memset(tempChr,0,datRead.count()+1);
    //        for(int x=0;x<datRead.count()+1;x++)
    //        {
    //            tempChr[x]=' ';
    //        }
    //        QString cc=ui->recipients->text();
    //        QByteArray dd;
    //        cc.replace("\r","");
    //        cc.replace("\n","");
    //        QMessageBox::information(this,"",cc);
    //        for(int x=0;x<cc.length();x++){
    //            dd.append(cc.at(x));
    //        }
    //        datRead.clear();
    //        datRead=file.readAll();
    //       QString tt;
    //       for(int x=0;x<datRead.count();x++){
    //           tt+=datRead.at(x);
    //       }
    //        QMessageBox::information(this,"",tt);
    //        file.write(tempChr,strlen(tempChr));
    //        delay(100);
    //        file.write(dd);
    //        stripKamera=cc;
    //        file.close();
    //    }

}

void MainWindow::errorMessage(const QString &message)
{
    //    QErrorMessage err (this);

    //    err.showMessage(message);

    //    err.exec();
}

void MainWindow::on_pbStartRecord_clicked()
{
    flagRekam=true;
    emit this->statusKamera("start");

}
void MainWindow::rekaman(){

    //    CvCapture* capture;

    //    capture = cvCreateCameraCapture(0);

    //    assert( capture != NULL );

    //    IplImage* bgr_frame = cvQueryFrame( capture );

    //    CvSize size = cvSize(
    //                (int)cvGetCaptureProperty( capture,
    //                                           CV_CAP_PROP_FRAME_WIDTH),
    //                (int)cvGetCaptureProperty( capture,
    //                                           CV_CAP_PROP_FRAME_HEIGHT)
    //                );

    //    cvNamedWindow( "Webcam ku", CV_WINDOW_AUTOSIZE );


    //    CvVideoWriter *writer = cvCreateVideoWriter(    "/media/TA/rekaman.AVI",
    //                                                    CV_FOURCC('D','I','V','X'),
    //                                                    30,
    //                                                    size
    //                                                    );

    //    int a =cvSaveImage("/home/pi/Desktop/project/2016/TA/ISTN/sony/kamera/tes.jpg",bgr_frame);

    //    qDebug()<< QString::number(a);
    //    while( (bgr_frame = cvQueryFrame( capture )) != NULL )
    //    {
    //        cvWriteFrame(writer, bgr_frame );
    //        cvShowImage( "Webcam ku", bgr_frame );

    //        char c = cvWaitKey( 33 );
    //        if( c == 27 ) break;
    //        if(!flagRekam)break;

    //    }

    //    cvReleaseVideoWriter( &writer );
    //    cvReleaseCapture( &capture );
    //    cvDestroyWindow( "Webcam ku" );
    //    qDebug()<< "Selesai merekam";
    VideoCapture cap(0); // open the video camera no. 0

    if (!cap.isOpened())  // if not success, exit program
    {
        cout << "ERROR: Cannot open the video file" << endl;
        // return -1;
    }


    namedWindow("MyVideo",CV_WINDOW_AUTOSIZE); //create a window called "MyVideo"

    double dWidth = cap.get(CV_CAP_PROP_FRAME_WIDTH); //get the width of frames of the video
    double dHeight = cap.get(CV_CAP_PROP_FRAME_HEIGHT); //get the height of frames of the video

    cout << "Frame Size = " << dWidth << "x" << dHeight << endl;

    Size frameSize(static_cast<int>(dWidth), static_cast<int>(dHeight));
    ui->lblStatusKamera->setText("merekam");

    VideoWriter oVideoWriter ("/media/ISTN/rekaman.AVI", CV_FOURCC('P','I','M','1'), 20, frameSize, true); //initialize the VideoWriter object

    if ( !oVideoWriter.isOpened() ) //if not initialize the VideoWriter successfully, exit the program
    {
        cout << "ERROR: Failed to write the video" << endl;
        //return -1;
    }

    while (1)
    {

        Mat frame;

        bool bSuccess = cap.read(frame); // read a new frame from video

        if (!bSuccess) //if not success, break loop
        {
            cout << "ERROR: Cannot read a frame from video file" << endl;
            break;
        }

        oVideoWriter.write(frame); //writer the frame into the file

        imshow("MyVideo", frame); //show the frame in "MyVideo" window

        imwrite( "/home/pi/Desktop/project/2016/TA/ISTN/sony/kamera/tes.jpg", frame );


        if (waitKey(10) == 27) //wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
        {
            cout << "esc key is pressed by user" << endl;
            break;
        }
        if(!flagRekam)break;
    }
    ui->lblStatusKamera->setText("tidak merekam");
}

void MainWindow::on_pbStopRecord_clicked()
{
    flagRekam=false;
}

void MainWindow::on_pbOn_clicked()
{

    pinMode(ui->txtPin->toPlainText().toInt(),OUTPUT);
    digitalWrite(ui->txtPin->toPlainText().toInt(),HIGH);
}

void MainWindow::on_pbOff_clicked()
{
    pinMode(ui->txtPin->toPlainText().toInt(),OUTPUT);
    digitalWrite(ui->txtPin->toPlainText().toInt(),LOW);
}

void MainWindow::on_pbReadInput_clicked()
{
    pinMode(ui->txtPin_INPUT->toPlainText().toInt(),INPUT);
    int a= digitalRead(ui->txtStatusInput->toPlainText().toInt());
    ui->txtStatusInput->setText(QString::number(a));
}

void MainWindow::on_pbOn_2_clicked()
{
    digitalWrite(R1,HIGH);
}

void MainWindow::on_pbOn_3_clicked()
{
    digitalWrite(R2,HIGH);
}

void MainWindow::on_pbOn_4_clicked()
{
    digitalWrite(R3,HIGH);
}

void MainWindow::on_pbOn_5_clicked()
{
    digitalWrite(R4,HIGH);
}

void MainWindow::on_pbOn_6_clicked()
{
    digitalWrite(R5,HIGH);
}

void MainWindow::on_pbOn_7_clicked()
{
    digitalWrite(R6,HIGH);
}

void MainWindow::on_pbOn_8_clicked()
{
    digitalWrite(R7,HIGH);
}

void MainWindow::on_pbOff_2_clicked()
{
    digitalWrite(R1,LOW);
}

void MainWindow::on_pbOff_3_clicked()
{
    digitalWrite(R2,LOW);
}

void MainWindow::on_pbOff_4_clicked()
{
    digitalWrite(R3,LOW);
}

void MainWindow::on_pbOff_5_clicked()
{
    digitalWrite(R4,LOW);
}

void MainWindow::on_pbOff_6_clicked()
{
    digitalWrite(R5,LOW);
}

void MainWindow::on_pbOff_7_clicked()
{
    digitalWrite(R6,LOW);
}

void MainWindow::on_pbOff_8_clicked()
{
    digitalWrite(R7,LOW);
}
int cntGlobal=0;
void MainWindow::modeOtomatis()
{
    if(ijinBuka==false){
        int proxPintu=digitalRead(btn1);
        int proxJendela=digitalRead(btn2);;
        int tomBolNetral=digitalRead(sensorReset);;
        int aktif=0;
        qDebug() << "Status reset=" + QString::number(tomBolNetral) +" Cnt="+QString::number(cntGlobal);
        cntGlobal++;
        if(terKunci==false )
        {
            ui->lblstatusTerkunci->setText("tidak terkunci");
            if( proxPintu==0 || proxJendela==0){ // 0 terkena objek prox
                terKunci=true;
                motorjendela(false);
                QDateTime dd;
                on_pbStart_clicked();
                QString q="Foto Maling, Waktu :"+dd.currentDateTime().toString("dd-MM-yyyy hh:mm:ss");
                ui->subject->setText(q);

            }
        }
        else if(terKunci==true && tomBolNetral==1 )// tombol d tekan =1
        {
            terKunci=false;
            ijinBuka=true;
            motorjendela(true);
            on_pbStop_clicked();
            ui->lblstatusTerkunci->setText("tidak terkunci");
    ui->lblstatusIjinMasuk->setText(" tidak aktif");

        }
    }
}

void MainWindow::on_btnReloadIPCAm_clicked()
{
    QString a=ui->txtIPCAM->toPlainText();
    ui->webView->load(QUrl(a));
    ui->webView->show();
    QFile file("ipkamera.txt");
    if(file.open(QIODevice::WriteOnly)){

        QString cc=ui->txtIPCAM->toPlainText();
        QByteArray dd;
        for(int x=0;x<cc.length();x++){
            dd.append(cc.at(x));
        }
        file.write(dd);
        stripKamera=cc;
        file.close();
    }
}


//void MainWindow::resultCameraMath(Mat image)
//{ Mat dest;
//    cvtColor(image, dest,CV_BGR2RGB);
//    QImage image1= QImage((uchar*) dest.data, dest.cols, dest.rows, dest.step, QImage::Format_RGB888);

//    //show Qimage using QLabel
//    ui->lblKameraLabel->setPixmap(QPixmap::fromImage(image1));
//    ui->lblKameraLabel->setScaledContents( true );
//    ui->lblKameraLabel->setSizePolicy( QSizePolicy::Ignored, QSizePolicy::Ignored );
//    //ui->lblKameraLabel->show();
//}


void MainWindow::on_pbStart_clicked()
{
    emit this->statusKamera("start");
    //  emit this->runKamera("D:\edy\latihan\qt\opencv\savevideocamera\tes.avi");
}

void MainWindow::on_pbStop_clicked()
{
    emit this->statusKamera("stop");
}
