#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "SmtpMime"
#include <QTimer>
#include <wiringPi.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cv.h>
#include <highgui.h>
#include "opencv2/opencv.hpp"

using namespace std;
using namespace cv;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    static EmailAddress * stringToEmail(const QString & str);

private slots:
    void on_addAttachment_clicked();

    void on_sendEmail_clicked();


    void on_pbStartRecord_clicked();

    void on_pbStopRecord_clicked();
    void updateTimer();
      void rekaman();
      void on_pbOn_clicked();

      void on_pbOff_clicked();

      void on_pbReadInput_clicked();

      void on_pbOn_2_clicked();

      void on_pbOn_3_clicked();

      void on_pbOn_4_clicked();

      void on_pbOn_5_clicked();

      void on_pbOn_6_clicked();

      void on_pbOn_7_clicked();

      void on_pbOn_8_clicked();

      void on_pbOff_2_clicked();

      void on_pbOff_3_clicked();

      void on_pbOff_4_clicked();

      void on_pbOff_5_clicked();

      void on_pbOff_6_clicked();

      void on_pbOff_7_clicked();

      void on_pbOff_8_clicked();

      void on_btnReloadIPCAm_clicked();
          void resultCameraMath(Mat);
          void on_pbStart_clicked();

          void on_pbStop_clicked();

signals:
      void runKamera(QString namaFile);
      void statusKamera(QString statusKamera);
private:
    Ui::MainWindow *ui;    void errorMessage(const QString & message);
    QTimer *timer1;
    void motorpintu(bool);
    void motorjendela(bool);
    void modeOtomatis();
    void FungsiRekam();
    bool terKunci;
    bool terKunciAwal;
    bool ijinBuka;
};

#endif // MAINWINDOW_H
